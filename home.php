<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Grid X : Home 3 style</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

        <!--Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,400italic%7CRaleway:400,600' rel='stylesheet' type='text/css'>

        <!--jQuery and plugins-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/underscore-min.js"></script>
        <script type="text/javascript" src="js/backbone-min.js"></script>
        <script type="text/javascript" src="js/backbone-paginated-collection.js"></script>
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="js/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="js/jquery.validate.min.js"></script>

        <!--Bootstrap-->
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>

        <!--Icons and Animates-->
        <link rel="stylesheet" href="plugins/elegant-font/style.css">
        <link rel="stylesheet" href="plugins/et-line-font/style.css">
        <link rel="stylesheet" href="css/animate.min.css">

        <!--Media Element Player-->
        <link rel="stylesheet" href="plugins/mediaelement/mediaelementplayer.min.css">
        <link rel="stylesheet" href="css/mediaelementplayer.css">
        <script type="text/javascript" src="plugins/mediaelement/mediaelement-and-player.min.js"></script>

        <!--Owl Carousel-->
        <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
        <script type="text/javascript" src="plugins/owl-carousel/owl.carousel.min.js"></script>

        <!--Magnific lightbox-->
        <link rel="stylesheet" href="plugins/magnific/magnific-popup.css">
        <script type="text/javascript" src="plugins/magnific/jquery.magnific-popup.min.js"></script>

        <!--Masterslider-->
        <link rel="stylesheet" href="plugins/masterslider/style/masterslider.css">
        <link rel="stylesheet" href="plugins/masterslider/skins/default/style.css">
        <link rel="stylesheet" href="css/masterslider.css">
        <script type="text/javascript" src="plugins/masterslider/jquery.easing.min.js"></script>
        <script type="text/javascript" src="plugins/masterslider/masterslider.min.js"></script>

        <!--Google Map-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQqYj4InzdbPwz-Kplwf9kltH5FrE5t5M&amp;sensor=false"></script>

        <!--Stylesheet-->
        <link rel="stylesheet" type="text/css" href="style.css">
        <!--Template: Item style 1 - COLORED CATEGORY-->
        <script type="text/template" id="tpl-item-style1">
            <div class="grid-item tpl-item-style1 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="background" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <% if (typeof(categories) != "undefined") { %>
            <a href="javascript:;" class="category" style="background-color:<%= _.last(categories).color %>;"><%= _.last(categories).title %></a>
            <% } %>
            <div class="entry-item">
            <span class="format <%= format %>"></span>
            <h2><a href="<%= link %>" class="post-permalink"><%= title %></a></h2>
            <span class="date"><%= date %></span>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 2 - SWAP IMAGE-->
        <script type="text/template" id="tpl-item-style2">
            <div class="grid-item tpl-item-style2 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <a href="<%= link %>" class="post-permalink" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);">
            <span style="background-image:url(<%= typeof(_logo)!== 'undefined' ?  _logo : '' %>);"></span>
            </a>
            </div>
            </div>
        </script>


        <!--Template: Item style 3 - BOOK COVER-->
        <script type="text/template" id="tpl-item-style3">
            <div class="grid-item tpl-item-style3 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="image" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <div class="hover">
            <div class="overlay" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>"></div>
            <span class="inner-table">
            <span class="inner-row"><h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3></span>
            </span>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 4 - PORTFOLIO with lightbox-->
        <script type="text/template" id="tpl-item-style4">
            <div class="grid-item tpl-item-style4 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="image" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <div class="hover">
            <div class="overlay" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>"></div>
            <span class="inner-table">
            <span class="inner-row">
            <span class="link-icon">
            <a href="javascript:;"><i class="glyphicon glyphicon-link"></i></a>&nbsp;
            <a href="javascript:;" class="popup"><i class="glyphicon glyphicon-search"></i></a>
            </span>
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            </span>
            </span>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 5 - TITLE CAT HOVER-->
        <script type="text/template" id="tpl-item-style5">
            <div class="grid-item tpl-item-style5 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="background" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <div class="entry-hover">
            <div class="inner-table">
            <div class="inner-row">
            <% if (typeof(categories) != "undefined") { %>
            <div class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="javascript:;"><%= _.escape(cat.title) %></a>
            <% }); %>
            </div>
            <% } %>
            <h2><a href="<%= link %>" class="post-permalink"><%= title %></a></h2>
            </div>
            </div>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 6 - CUBIC HOVER-->
        <script type="text/template" id="tpl-item-style6">
            <div class="grid-item tpl-item-style6 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);">
            <div class="entry-hover">
            <div class="inner-table">
            <div class="inner-row">
            <h2><a href="<%= link %>" class="post-permalink"><%= title %></a></h2>
            <% if (typeof(categories) != "undefined") { %>
            <div class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="javascript:;"><%= _.escape(cat.title) %></a> 
            <% }); %>
            </div>
            <% } %>
            </div>
            </div>
            </div>
            </div>
            </div>
        </script>



        <!--Template: Item style 7 - GRAY SCALE-->
        <script type="text/template" id="tpl-item-style7">
            <div class="grid-item tpl-item-style7 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <a href="<%= link %>" class="post-permalink" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);">
            <span style="background-image:url(<%= typeof(_logo)!== 'undefined' ?  _logo : '' %>);"></span>
            </a>
            </div>
            </div>
        </script>


        <!--Template: Item style 8 - TITLE BOTTOM HOVER-->
        <script type="text/template" id="tpl-item-style8">
            <div class="grid-item tpl-item-style8 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="image" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <div class="hover">
            <span class="inner-table">
            <span class="inner-row">
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            <% if (typeof(categories) != "undefined") { %>
            <div class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="javascript:;"><%= _.escape(cat.title) %></a> 
            <% }); %>
            </div>
            <% } %>
            </span>
            </span>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 9 - FLIP EFFECT-->
        <script type="text/template" id="tpl-item-style9">
            <div class="grid-item tpl-item-style9 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="front" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"></div>
            <div class="back">
            <span class="inner-table">
            <span class="inner-row">
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            <% if (typeof(categories) != "undefined") { %>
            <div class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="javascript:;"><%= _.escape(cat.title) %></a> 
            <% }); %>
            </div>
            <% } %>
            </span>
            </span>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 10 - AUTO FLIP EFFECT-->
        <script type="text/template" id="tpl-item-style10">
            <div class="grid-item tpl-item-style10 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="front" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"><a href="#" class="post-permalink"></a></div>
            <div class="back" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"><a href="#" class="post-permalink"></a></div>
            </div>
            </div>
        </script>
        <script type="text/template" id="tpl-item-style11">
            <div class="grid-item tpl-item-style11 <%= typeof(grid_size)!== 'undefined' ?  grid_size : '' %>" data-id="<%= id %>" style="<% if (typeof(categories) != "undefined") { %>background-color:<%= _.last(categories).color %>;<% } %>">
            <div class="grid-item-entry">
            <div class="front" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"><a href="#" class="post-permalink"></a></div>
            <div class="back" style="background-image:url(<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>);"><a href="#" class="post-permalink"></a></div>
            </div>
            </div>
        </script>


        <!--Template: Item style 1 - MASONRY FOR PORTFOLIO-->
        <script type="text/template" id="tpl-masonry-style1">
            <div class="grid-item tpl-masonry-style1" data-id="<%= id %>">
            <div class="grid-item-entry">
            <div class="entry-media">
            <a href="<%= link %>" class="post-permalink"><img src="<%= typeof(thumb)!== 'undefined' ?  thumb : '' %>" ></a>
            </div>
            <div class="entry-meta">
            <h2><a href="<%= link %>" class="post-permalink"><%= title %></a></h2>
            <% if (typeof(categories) != "undefined") { %>
            <div class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="<%= _.escape(cat.link) %>"><%= _.escape(cat.title) %></a>
            <% }); %>
            </div>
            <% } %>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 2 - MASONRY FOR POST FORMAT-->
        <script type="text/template" id="tpl-masonry-style2">
            <div class="grid-item tpl-masonry-style2" data-id="<%= id %>">
            <div class="grid-item-entry entry-format-<%= format %>">
            <% if (typeof(media) != "undefined") { %>
            <div class="entry-media"><%= media %></div>
            <% } %>
            <div class="entry-meta">
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            <div class="meta">
            <% if (typeof(categories) != "undefined") { %>
            <span class="categories">
            <i class="icon_tag_alt"></i>
            <a href="<%= _.last(categories).link %>"><%= _.last(categories).title %></a> 
            </span>
            <% } %>
            <span class="date">
            <i class="icon_clock_alt"></i><%= date %>
            </span>
            </div>
            <div class="excerpt"><%= excerpt %></div>
            </div>
            </div>
            </div>
        </script>


        <!--Template: Item style 3 - MASONRY-->
        <script type="text/template" id="tpl-masonry-style3" data-id="<%= id %>">
            <div class="grid-item tpl-masonry-style3">
            <div class="grid-item-entry">
            <% if (typeof(thumb) != "undefined") { %>
            <div class="entry-media">
            <a href="<%= link %>"><img src="<%= thumb %>" ></a>
            </div>
            <% } %>
            <div class="entry-meta">
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            <div class="meta">
            <% if (typeof(categories) != "undefined") { %>
            <span class="categories">
            <% _.each(categories, function(cat){ %>
            <a href="<%= _.escape(cat.link) %>"><%= _.escape(cat.title) %></a>
            <% }); %>
            </span>
            <% } %>
            </div>
            </div>
            </div>
            </div>
        </script><!--Template: Push State-->
        <script type="text/template" id="tpl-single-open-state">
            <div class="open-state viewport-single-post">
            <div class="entry-media"><%= typeof(media)!== 'undefined' ?  media : '' %></div>
            <div class="post-action">
            <span class="actions">
            <a href="javascript:;" class="prev">
            <i class="switch-close arrow_carrot-left"></i>
            <i class="switch-holder icon_grid-3x3"></i>
            </a>
            <a href="javascript:;" class="next"><i class="arrow_carrot-right"></i></a>
            <a href="javascript:;" class="close"><i class="icon_close"></i></a>
            </span>
            </div>
            <article>
            <h2 class="post-title">
            <%= title %>
            <span class="share">
            <a href="#">
            <i class="switch-share icon_link_alt"></i>
            <i class="switch-item social_facebook"></i>
            </a>
            <a href="#"><i class="social_googleplus"></i></a>
            <a href="#"><i class="social_twitter"></i></a>
            <a href="#"><i class="social_pinterest"></i></a>
            </span>
            </h2>
            <%= content %>
            <div class="meta row">
            <div class="col-md-6">
            <% if (typeof(categories) != "undefined") { %>
            <ul class="categories">
            <% _.each(categories, function(cat){ %>
            <li><a href="<%= _.escape(cat.link) %>"><%= _.escape(cat.title) %></a></li>
            <% }); %>
            </ul>
            <% } %>
            </div>
            <div class="col-md-6 text-right">
            <div class="date"><%= date %> <span class="comments">0 comments</span></div>
            </div>
            </div>
            </article>
            </div>
        </script>


        <!--Template: Open GI-->
        <script type="text/template" id="tpl-single-open-gi">
            <div class="grid-open-gi">
            <div class="panel-gi row">
            <div class="col-md-7">
            <div class="entry-media">
            <%= typeof(media)!== 'undefined' ?  media : '' %>
            </div>
            </div>
            <div class="col-md-5">
            <article>
            <h2 class="post-title"><%= title %></h2>
            <p><%= excerpt %></p>
            </article>

            <a href="javascript:;" class="item-action prev"><i class="arrow_carrot-left"></i></a>
            <a href="javascript:;" class="item-action next"><i class="arrow_carrot-right"></i></a>
            </div>
            </div>
            <a href="javascript:;" class="close-viewport"><i class="icon_close"></i></a>
            </div>
        </script>


        <!--Template: Modal-->
        <script type="text/template" id="tpl-single-modal">
            <div id="gx-single-modal" class="modal single-modal fade" role="dialog" aria-labelledby="single-modal-label" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-body"></div>
            </div>
            </div>
            </div>
        </script>



        <!--Template: Header Search-->
        <script type="text/template" id="tpl-header-search">
            <div class="search-template">
            <div class="inner-table">
            <div class="inner-row">
            <div class="container">
            <form role="search" method="get" class="search-form" action="blog.html">
            <div class="input-group">
            <input type="search" class="search-field" placeholder="Type and hit Enter ..." value="" name="s" autocomplete="off">
            <input type="submit" class="search-submit" value="Go">
            <a href="javascript:;" class="close-search"><i class="icon_close"></i></a>
            </div>
            </form>
            </div>
            </div>
            </div>
            </div>
        </script>    </head>
    <body class="header-styled header-transparent header-fixed-top menu-light">

        <!-- Document wrapper
        ================================================== -->
        <div class="wrapper">

            <!-- Header
            ================================================== -->
            <header id="header" class="navbar navbar-default">
                <div class="container">

                    <!-- Logo
                    ================================================== -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="index.html" style="background-image:url(images/logo-light.png);">Grid X</a>
                        <a class="navbar-brand retina" href="index.html" style="background-image:url(images/logo-light@2x.png);">Grid X</a>
                    </div>

                    <!-- Main navigation
================================================== -->
                    <nav>
                        <ul class="nav navbar-nav main-menu">
                            <li class="active"><a href="index.html">Home</a>
                                <ul>
                                    <li><a href="home-1.html">Home 1 - Fullscreen</a></li>
                                    <li><a href="home-2.html">Home 2 - Left Menu</a></li>
                                    <li><a href="home-3.html">Home 3 - Flat</a></li>
                                    <li><a href="home-4.html">Home 4 - Business</a></li>
                                    <li><a href="home-5.html">Home 5 - Business 2</a></li>
                                    <li><a href="home-6.html">Home 6 - Polygon</a></li>
                                    <li><a href="home-7.html">Home 7 - Book</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="grid-styles.html">Grid X</a>
                                <ul>
                                    <li><a href="grid-styles.html">All In A Page</a></li>
                                    <li>
                                        <a href="grid.html?grid=x&amp;style=1">Grid X - Styles</a>
                                        <ul>
                                            <li><a href="grid.html?grid=x&amp;style=1">Style 1</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=2">Style 2</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=3">Style 3</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=4">Style 4</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=5">Style 5</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=6">Style 6</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=7">Style 7</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=8">Style 8</a></li>
                                            <li><a href="grid.html?grid=x&amp;style=9">Style 9</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="grid.html?grid=style&amp;style=1">Grid X - Styles</a>
                                        <ul>
                                            <li><a href="grid.html?grid=style&amp;style=1">Style 1</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=2">Style 2</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=3">Style 3</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=4">Style 4</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=5">Style 5</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=6">Style 6</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=7">Style 7</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=8">Style 8</a></li>
                                            <li><a href="grid.html?grid=style&amp;style=9">Style 9</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="grid.html?grid=masonry&amp;style=1">Masonry - Styles</a>
                                        <ul>
                                            <li><a href="grid.html?grid=masonry&amp;style=1">Style 1</a></li>
                                            <li><a href="grid.html?grid=masonry&amp;style=2">Style 2</a></li>
                                            <li><a href="grid.html?grid=masonry&amp;style=3">Style 3</a></li>
                                        </ul>
                                    </li>

                                    <li class="divider"></li>
                                    <li><a href="grid.html?grid=x&amp;style=3&amp;open=push-state">Opeing - Push State</a></li>
                                    <li><a href="grid.html?grid=style&amp;style=3&amp;open=open-gi">Opeing - Open GI</a></li>
                                    <li><a href="grid.html?grid=style&amp;style=3">Opeing - Modal</a></li>

                                    <li class="divider"></li>
                                    <li><a href="grid-1.html">Grid Demo - 1</a></li>
                                    <li><a href="grid-2.html">Grid Demo - 2</a></li>
                                    <li><a href="grid-3.html">Grid Demo - 3</a></li>
                                    <li><a href="grid-4.html">Grid Demo - 4</a></li>
                                    <li><a href="grid-5.html">Grid Demo - 5</a></li>
                                    <li><a href="grid-6.html">Grid Demo - 6</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="page.html">Pages</a>
                                <ul>
                                    <li><a href="footers.html">Footer Styles</a></li>
                                    <li><a href="single.html">Single Pages</a>
                                        <ul>
                                            <li><a href="single.html">Post Single</a></li>
                                            <li><a href="portfolio-single.html">Portfolio Single</a></li>
                                            <li><a href="portfolio-single-full.html">Portfolio Single Fullwidth</a></li>
                                            <li><a href="page.html">Page</a></li>
                                            <li><a href="page-right.html">Page Right</a></li>
                                            <li><a href="page-left.html">Page Left</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="contact-us.html">Contact Us</a></li>
                                    <li><a href="shortcodes.html">Shortcodes</a></li>
                                    <li><a href="blog.html">Regular Blog</a></li>

                                    <li class="divider"></li>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="grid-logo.html">+ Logo Page</a></li>
                                    <li><a href="grid-team.html">Team Page</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="headers.html">Headers</a>
                                <ul>
                                    <li><a href="headers.html">All In A Page</a></li>
                                    <li><a href="home-4.html">Regular</a></li>
                                    <li><a href="home-1.html">Fullscreen menu</a></li>
                                    <li><a href="home-2.html">Left side</a></li>
                                    <li><a href="home-5.html">Transparent</a></li>
                                    <li><a href="header-transparent-bordered.html">Transparent Bordered</a></li>
                                    <li><a href="grid-4.html">Filled Style</a></li>
                                    <li><a href="headers.html#left">Align Left</a></li>
                                    <li><a href="grid-1.html">Align Center</a></li>
                                    <li><a href="headers.html#right">Align Right</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="doc.html">Doc</a>
                            </li>
                        </ul>

                        <!-- Top links
                        ================================================== -->
                        <ul class="nav navbar-nav navbar-right">
                            <li class="header-search-form"><a href="javascript:;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></li>
                            <li class="header-shop-cart"><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a></li>
                            <li class="header-menu-icon"><a class="nav-main-trigger" href="javascript:;"><span class="nav-menu-icon"></span></a></li>
                        </ul>
                    </nav>                </div>
            </header>
			
			
			<!-- Page title
            ================================================== -->
            <!-- content deleted by Karan -->

            <!-- Top slider
            ================================================== -->
            <section class="top-slider top-slider-3" >
                <div class="slider-background" style="background-color: #83a0be; ">
                
                </div>
                <!-- masterslider -->
                <div class="master-slider ms-skin-default" id="masterslider_01">

                    <div class="ms-slide slide-1">
                        <div class="ms-layer text-center slider-caption" data-type="text" data-effect="top(45)" data-duration="1000" data-ease="easeOutExpo">
                            <h2 class="big-text text-white bold-text no-margin">GRID X THEME</h2>
                            <p class="text-bold text-white thin-text small-text">
                                Continually re-engineer orthogonal users for maintainable collaboration and idea-sharing.<br> Authoritatively strategize open-source value through revolutionary deliverables.<br> Intrinsicly evolve optimal.
                            </p>
                            <button class="btn btn-success styled" style="border-width:2px; margin-right:10px;">PURCHASE NOW</button>
                            <button class="btn glass btn-success">LEARN MORE</button>
                        </div>

                    </div>
                    

                </div>
                <!-- end of masterslider -->

 
               
            </section>

            <!-- Content
            ================================================== -->
            <section class="content no-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

<?php /*
                            <!-- Fullwidth section
                            ================================================== -->
                            <div class="fullwidth-section padding2x" style="background-color:rgba(0,0,0,.04);">
                                <div class="container">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center style-light">
                                                <h2 class="heading-dash">Objectively synthesize unique methods of empowerment</h2>
                                                <div class="space" data-height="8"></div>
                                                <h4>
                                                    Progressively reinvent bricks-and-clicks solutions before impactful results.<br>
                                                    Intrinsicly revolutionize holistic data rather than real-time users.
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6 col-md-3">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-01.svg" width="80" alt="Icon">
                                                </div>
                                                <h3>Service Quality</h3>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-02.svg" width="80" alt="Icon">
                                                </div>
                                                <h3>Progressively reinvent</h3>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-03.svg" width="80" alt="Icon">
                                                </div>
                                                <h3>Driven e-tailers</h3>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-04.svg" width="80" alt="Icon">
                                                </div>
                                                <h3>Intrinsicly</h3>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Fullwidth section
                            ================================================== -->
                            <div class="fullwidth-section padding2x text-light" style="background-color:#fd5f42;">
                                <div class="container">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <h1>how we work</h1>
                                            </div>

                                            <div class="text-center style-light">
                                                <h4 class="heading-dash">Compellingly administrate resource sucking experiences before cross functional products. Assertively target<br>unique"outside the box" thinking after interactive partnership style-lights.</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-44.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>01. Service Quality</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-58.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>02. Progressively reinvent</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-06.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>03. Driven e-tailers</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
*/ ?>

                            <!-- Fullwidth section
                            ================================================== -->
                            <div class="fullwidth-section" style="background-color:rgba(0,0,0,.04);">
                                <div class="container">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <h1>our portfolio</h1>
                                            </div>
                                            <div class="text-center style-light">
                                                <h4 class="heading-dash">
                                                    Credibly transform client-based ideas before compelling infomediaries. Completely provide access to turnkey<br> interfaces and empowered benefits. Uniquely innovate exceptional.
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="grid_layout_id" class="grid-container grid-masonry" data-item-style="tpl-masonry-style2" data-column="3" data-open-state="modal" data-size-gutter="25" data-pager="6">
    <div class="grid-viewport row"></div>
    <script type="text/template" class="grid-data">{
    "posts": [
        
        {
            "id": 141,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        },
        {
            "id": 142,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        },
        {
            "id": 143,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        },
        {
            "id": 144,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        },
        {
            "id": 145,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        },
        {
            "id": 146,
            "title": "Intrinsicly redefine superior",
            "date": "January 8 sdcsd, 2015",
            "format": "standard",
            "media": "<img src=\"images/demo-images/5.png\" >",
            "link": "#",
            "grid_size": "normal",
            "thumb": "images/demo-images/5.png",
            "excerpt": "Interactively seize client-focused best practices through compelling e-tailers. Distinctively transition team driven best practices whereas synergistic imperatives.",
            "content": "",
            "_logo": "images/logo/5.png",
            "categories": [
                {
                    "cat_id": 0,
                    "title": "Web Design",
                    "color": "#1D62F0",
                    "slug": "category_0",
                    "link": "#"
                },
                {
                    "cat_id": 5,
                    "title": "Development",
                    "color": "#34AADC",
                    "slug": "category_5",
                    "link": "#"
                }
            ]
        }

        
    ]
}</script>
</div>

<script type="text/template" id="tpl-masonry-style2">
            <div class="grid-item tpl-masonry-style2" data-id="<%= id %>">
            <div class="grid-item-entry entry-format-<%= format %>">
            <% if (typeof(media) != "undefined") { %>
            <div class="entry-media"><%= media %></div>
            <% } %>
            <div class="entry-meta">
            <h3><a href="<%= link %>" class="post-permalink"><%= title %></a></h3>
            <div class="meta">
            <% if (typeof(categories) != "undefined") { %>
            <span class="categories">
            <i class="icon_tag_alt"></i>
            <a href="<%= _.last(categories).link %>"><%= _.last(categories).title %></a> 
            </span>
            <% } %>
            <span class="date">
            <i class="icon_clock_alt"></i><%= date %>
            </span>
            </div>
            <div class="excerpt"><%= excerpt %></div>
            </div>
            </div>
            </div>
        </script>
                            </div>

<?php /*
                            <!-- Fullwidth section
                            ================================================== -->
                            <div class="fullwidth-section padding2x img-center-bottom text-light" style="background-color:#8b6c93;" data-image="images/home3/horizontal-com.png">
                                <div class="container">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <h1>creativity</h1>
                                            </div>

                                            <div class="text-center style-light">
                                                <h4 class="heading-dash">
                                                    Objectively disintermediate interdependent technologies via an expanded array of markets. Credibly<br> implement diverse vortals through flexible resources. Uniquely deliver.
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-18.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>01. Service Quality</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-19.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>02. Progressively reinvent</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="service-box style-center">
                                                <div class="service-icon">
                                                    <img src="images/svg-icons/set-20.svg" width="80" alt="Icon">
                                                </div>
                                                <h4>03. Driven e-tailers</h4>
                                                <p>Professionally promote performance based e-commerce rather than team driven e-tailers.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space" data-height="350"></div>
                                </div>
                            </div>
*/ ?>

                            <!-- Fullwidth section
                            ================================================== -->
                            <div class="fullwidth-section padding2x text-light" style="background-color:#98bb56;">
                                <div class="container">

                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="testimonials-slider">
                                                <div class="testimonial">
                                                    <blockquote>
                                                        <p>My favorite things in life don't cost any money. It's really clear that the most precious resource we all have is time.</p>
                                                        <footer>Steve Jobs <span>Apple Inc.</span></footer>
                                                    </blockquote>
                                                </div>
                                                <div class="testimonial">
                                                    <blockquote>
                                                        <p>Some people think design means how it looks. But of course, if you dig deeper, it's really how it works.</p>
                                                        <footer>Steve Jobs <span>Apple Inc.</span></footer>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div><!-- .row -->
                </div><!-- .container -->
            </section><!-- .content -->

            <!-- Footer
================================================== -->
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <aside class="widget widget_instagram clearfix">
                                <h3 class="widget-title">Instagram Photos</h3>
                                <ul class="list-inline">
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60-1.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60-2.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                    <li><a class="widget-thumb" href="#"><img src="images/thumb/60x60.png" alt="Thumb"/></a></li>
                                </ul>
                            </aside>
                        </div><!-- .col-md-3 -->
                        <div class="col-sm-6 col-md-3">
                            <aside class="widget widget_recent_news">
                                <h3 class="widget-title">Latest News</h3>
                                <ul>
                                    <li>
                                        <a class="widget-thumb" href="#"><img src="images/thumb/60x60-1.png" alt="Thumb"/></a>
                                        <div class="widget-content">
                                            <a href="#">Unique utilize transfer</a>
                                            <p>Proactively exploit seamless strategic theme areas with timely initiatives conveniently you.</p>
                                            <span class="post-date">January 2, 2015</span>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="widget-thumb" href="#"><img src="images/thumb/60x60-2.png" alt="Thumb"/></a>
                                        <div class="widget-content">
                                            <a href="#">HTML Tags and Formatting</a>
                                            <p>Proactively exploit seamless strategic theme areas with timely initiatives conveniently you.</p>
                                            <span class="post-date">October 11, 2014</span>
                                        </div>
                                    </li>
                                </ul>
                            </aside>
                        </div><!-- .col-md-3 -->
                        <div class="col-sm-6 col-md-3">
                            <aside class="widget widget_recent_entries">
                                <h3 class="widget-title">Recent Posts</h3>
                                <ul>
                                    <li>
                                        <a href="#">Compellingly network e-business value before unique low risk leardership markets</a>
                                        <span class="post-date">January 2, 2015</span>
                                    </li>
                                    <li>
                                        <a href="#">Markup: HTML Tags and Formatting</a>
                                        <span class="post-date">October 11, 2014</span>
                                    </li>
                                    <li>
                                        <a href="#">Image Alignment</a>
                                        <span class="post-date">March 10, 2014</span>
                                    </li>
                                    <li>
                                        <a href="#">Markup: Title With Special Characters</a>
                                        <span class="post-date">December 5, 2013</span>
                                    </li>
                                </ul>
                            </aside>
                        </div><!-- .col-md-3 -->
                        <div class="col-sm-6 col-md-3">
                            <aside class="widget widget_social clearfix">
                                <h3 class="widget-title">Socials</h3>
                                <ul>
                                    <li><a href="#" class="social_facebook" title="Facebook"></a></li>
                                    <li><a href="#" class="social_twitter"></a></li>
                                    <li><a href="#" class="social_pinterest"></a></li>
                                    <li><a href="#" class="social_instagram"></a></li>
                                    <li><a href="#" class="social_googleplus"></a></li>
                                </ul>
                            </aside>
                            <aside class="widget widget_tag_cloud clearfix">
                                <h3 class="widget-title">Tags</h3>
                                <div class="tagcloud">
                                    <a href="#" class="tag-link" title="6 topic">8BIT</a>
                                    <a href="#" class="tag-link" title="12 topic">Articles</a>
                                    <a href="#" class="tag-link" title="1 topic">aside</a>
                                    <a href="#" class="tag-link" title="1 topic">audio</a>
                                    <a href="#" class="tag-link" title="2 topics">captions</a>
                                    <a href="#" class="tag-link" title="2 topics">categories</a>
                                    <a href="#" class="tag-link" title="23 topics">chat lenght</a>
                                    <a href="#" class="tag-link" title="3 topics">Codex</a>
                                    <a href="#" class="tag-link" title="3 topics">ThemeForest</a>
                                </div><!-- .tagcloud -->
                            </aside>
                        </div><!-- .col-md-3 -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </footer>

            <!-- Sub Footer
            ================================================== -->
            <div id="sub-footer" class="sub-footer">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-6">
                            Copyright &copy; 2015 All Rights Reserved by <a href="#">ThemeTon</a> Corp.
                        </div>

                        <div class="col-sm-6 text-right">
                            <ul class="footer-menu list-inline">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Inspiration</a></li>
                                <li><a href="#">Interview</a></li>
                                <li><a href="#">Review</a></li>
                            </ul>
                        </div>

                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- sub-footer -->
            <!-- Back to top anchor -->
            <span class="back-to-top"></span>
        </div><!-- end .wrapper -->

        <!-- Javascript -->
        <script type="text/javascript" src="js/grid.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>
</html>